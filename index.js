let number = prompt("Enter a number");

while(isNaN(number) || number == "") {
	number = prompt("Enter a proper  number. Thank you.");
}

for ( ; number > 0 ; number--) {

	if(number % 5 == 0) {
		console.log(number);
	}
	
	if(number % 10 == 0) {
		console.log("This number is being skipped: " + number);
		continue;
	}
	if(number <= 50)
		break;
}

let longWord = "supercalifragilisticexpialidocious";
let store = "";
for(let i = 0; i < longWord.length; i++) {
	if (longWord[i].toLowerCase() == 'a' ||
		longWord[i].toLowerCase() == 'e' ||
		longWord[i].toLowerCase() == 'i' ||
		longWord[i].toLowerCase() == 'o' || 
		longWord[i].toLowerCase() == 'u') {
		continue;
	}
	else {
		store += longWord[i];
	}
}

console.log(store);